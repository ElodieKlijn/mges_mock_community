#!/bin/bash
source /home/dla_mm/eklijn/data/miniconda3/etc/profile.d/conda.sh
conda activate quast
#$ -S /bin/bash
#$ -cwd
#$ -M e.v.klijn-3@umcutrecht.nl
#$ -m beas
# Request resources (64GB, 8 threads and 8 wallclock-hours)
#$ -l h_vmem=64G
#$ -l h_rt=8:00:00
#$ -pe threaded 8
#Logs
#$ -o ./output_messages/quast_ourassembly_output.txt
#$ -e ./error_messages/quast_ourassembly_error.txt

quast.py \
../assemblies/our_assembly.fasta \
-o ../data/quast/our_assembly \
--glimmer 2>> {log.stderr} 1>> {log.stdout}
