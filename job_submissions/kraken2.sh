#!/bin/bash
source /home/dla_mm/eklijn/data/miniconda3/etc/profile.d/conda.sh
conda activate kraken2
#$ -S /bin/bash
#$ -cwd
#$ -M e.v.klijn-3@umcutrecht.nl
#$ -m beas
#Request resources (160GB, 8 threads and 12 wallclock-hours)
#$ -l h_vmem=160G
#$ -l h_rt=12:00:00
#$ -pe threaded 8
#Logs:
#$ -o ../data/kraken2/funbac_results/kraken2_contigs_funbac_output.txt
#$ -e ./error_messages/kraken2_contigs_funbac1_error.txt

#Run tools here
kraken2 --db ../data/kraken2/db_funbac \
--report ../data/kraken2/funbac_results/contigs_funbac_report.txt ../assemblies/our_assembly.fasta 

