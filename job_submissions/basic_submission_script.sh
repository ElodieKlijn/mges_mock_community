#!/bin/bash
source /home/dla_mm/eklijn/data/miniconda3/etc/profile.d/conda.sh
conda activate mock_community
#$ -S /bin/bash
#$ -cwd
#$ -M e.v.klijn-3@umcutrecht.nl
#$ -m beas
#Request resources (64GB, 8 threads and 8 wallclock-hours)
#$ -l h_vmem=64G
#$ -l h_rt=8:00:00
#$ -pe threaded 8
#Logs:
#$ -o ../data/basic_output.txt
#$ -e ./error_messages/basic_error.txt

#Run tools here
