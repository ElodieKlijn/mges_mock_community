All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                        contigs         
# contigs (>= 0 bp)             55957           
# contigs (>= 1000 bp)          1514            
# contigs (>= 5000 bp)          487             
# contigs (>= 10000 bp)         433             
# contigs (>= 25000 bp)         308             
# contigs (>= 50000 bp)         188             
Total length (>= 0 bp)          47488817        
Total length (>= 1000 bp)       31640458        
Total length (>= 5000 bp)       30105935        
Total length (>= 10000 bp)      29727220        
Total length (>= 25000 bp)      27608666        
Total length (>= 50000 bp)      23358912        
# contigs                       7638            
Largest contig                  531082          
Total length                    35679152        
GC (%)                          48.23           
N50                             85250           
N75                             30098           
L50                             105             
L75                             278             
# N's per 100 kbp               0.00            
# predicted genes (unique)      24231           
# predicted genes (>= 0 bp)     23820 + 438 part
# predicted genes (>= 300 bp)   21281 + 380 part
# predicted genes (>= 1500 bp)  4148 + 16 part  
# predicted genes (>= 3000 bp)  644 + 3 part    
