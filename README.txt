Mobile genetic element classification in metagenomic sequences

The aim of this project was to identify and annotate mobile genetic elements (MGEs) in a mock community, using the currently available tools.

The mock community data is from the ZymoBIOMICS Microbial Community Standards and raw read data was published by: Nicholls et al. (2019), 'Ultra-deep, long-read nanopore sequencing of mock microbial community standards.' 

Most of this work was performed on the HPC and the data can be found at the following path:
/home/dla_mm/eklijn/data/mock_community_mges/report_data/
It contains folders for the assemblies, data (output), databases and job submissions. 

In short, the raw read data (made with Illumina sequencing technique) was used to make an assembly with metaSPAdes. Different MGEs identifying tools were then used to identify the MGEs in the assembly, including: PlasFLow, Virsorter and ISEscan. From the PlasFlow results, antimicrobial resistance and virulence genes were identified with Abricate. Kraken2 was used to determine how many of the contigs in the assembly were assigned to each of the species present in the mock community.

The job submissions folder contains the scripts that were used for running the tools. The different assemblies can be found in the assemblies folder, such as our assembly, the original assembly from Nicholls et al. and our assemblies when the minimum contig length is 500 kb (filtered_contigs500.fasta) or 1000 kb (filtered_contigs1000.fasta).
All data (output) can be found in the data folder at the HPC, and most of it can also be found on GitLab. However, the larger files could not be uploaded. These files are:
	- the raw reads with which the assembly was made (~/data/raw_reads folder on the HPC)
	- the Trimmomatic output files
	- the ~/data/metaSPAdes/corrected folder
	- the databases used for Virsorter and Kraken2

The databases for Virsorter and Kraken2 can both be found on the HPC. The Virsorter database can be found at ~/data/databases/ . The database for kraken2 (inlcuding fungi and bacteria) and information how it was generated can be found here: /hpc/dla_mm/kraken2_db_fungi_bacteria/ .

In case there any questions, please do not hesisate to contact me: e.v.klijn@gmail.com

Last modified: 01/01/2020 by Elodie  
